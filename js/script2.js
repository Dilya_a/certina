

		var sliderLeftBlock  = $('.slider_left_block'),
			sliderTableBlock = $('.slider_table_block'),
			buttonsSlide     = $('.buttons_slide');


		if(sliderLeftBlock.length){
			include("plugins/owl-carousel/owl.carousel.js");
		}
		if(sliderTableBlock.length){
			include("plugins/flexslider/jquery.flexslider.js");
		}

		function include(url){ 

			document.write('<script src="'+ url + '"></script>'); 

		}


		$(document).ready(function(){

			if(sliderTableBlock.length){
				
				sliderTableBlock.flexslider({
			    	animation: "slide",
			    	controlNav: true,
			    	animationLoop: false,
			    	slideshow: false,
			    	mousewheel: false,
			    	smoothHeight: true,
			    	prevText: "<",
			    	nextText: ">",
			    	touch: false,
			    	directionNav: true,
			    	init:  function() {
			    		setTimeout(function(){

				    		var listOl = $('.flex-control-nav').width(),
				    			listUl = $('.flex-direction-nav');

			    			listUl.css({
			    				"width" : listOl + 15
			    			});

			    		},500);
			    	}
			  	});

			}

			if(sliderLeftBlock.length){
				var owl = sliderLeftBlock.owlCarousel({
				    navigation:true,
					singleItem : true,
					paginationSpeed : 1000,
					goToFirstSpeed : 2000
				});
			}


			if(buttonsSlide.length){
				var counter          = 0,
					buttonsSlides    = $('.buttons_slide'),
					buttonClick      = buttonsSlides.find("button");

					buttonClick.each(function(){
						counter += 1;
						$(this).addClass('index_hash'+counter+'');
					});

					buttonClick.on('click', function(){
						buttonClick.removeClass('current');
						$(this).addClass('current');
						sliderLeftBlock.data('owlCarousel').goTo($(this).index());
					});

			}


		});