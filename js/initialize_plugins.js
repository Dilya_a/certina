//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins START  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================*/




		var tabs        = $('.tabs_container'),
			matchheight = $("[data-mh]"),
		    styler      = $(".styler"),
			scroll      = $(".scroll_container"),
			mouse       = $(".mouse"),
			windowW     = $(window).width(),
			windowH     = $(window).height();


			if(matchheight.length){
					include("plugins/jquery.matchHeight-min.js");
			}
			if(styler.length){
					include("plugins/formstyler/formstyler.js");
			}
			if(tabs.length){
					include("plugins/easy-responsive-tabs/easyResponsiveTabs.js");
			}
			if(scroll.length || mouse.length){
		      include("plugins/scroll-pane/jquery.jscrollpane.js"); 
		      include("plugins/scroll-pane/jquery.mousewheel.js"); 
		    };

					include("plugins/modernizr.js");



			function include(url){ 

					document.write('<script src="'+ url + '"></script>'); 

			}

		


		$(document).ready(function(){




			/* ------------------------------------------------
			FORMSTYLER START
			------------------------------------------------ */

					if (styler.length){
						styler.styler({
							selectSmartPositioning: true
						});
					}

			/* ------------------------------------------------
			FORMSTYLER END
			------------------------------------------------ */




			/* ------------------------------------------------
			TABS START
			------------------------------------------------ */

					if(tabs.length){
						tabs.easyResponsiveTabs();
					}

			/* ------------------------------------------------
			TABS END
			------------------------------------------------ */




			/* ------------------------------------------------
			SCROLL START
			------------------------------------------------ */


					if(scroll.length || mouse.length){


						scroll.each(function(){
					        var $this = $(this);
					        if($this.hasClass('arr_top_down')){
					          $this.jScrollPane({
					            showArrows: true
					          });
					        }
					        else{
					          $this.jScrollPane({
					            showArrows: false
					          });
					        }
					      });


						mouse.mousewheel(); 


					}

			/* ------------------------------------------------
			SCROLL END
			------------------------------------------------ */




		});




//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins END    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================
